import streamlit as st
import time
from streamlit_option_menu import option_menu
from bot import finance_bot

# Demo app to showcase the bot

st.set_page_config(page_title="Ask me anything", layout="wide")



def load_config():
    import yaml
    with open("config.yaml", "r") as stream:
        try:
            return yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)
            

def main():

    selected = option_menu(None, ["Home", "Banking", "Tasks", 'Settings'],
        icons=['house', 'piggy-bank', "list-task", 'gear'],
        menu_icon="cast", default_index=0, orientation="horizontal")


    col_1, col_2 = st.columns([0.14, 0.9])
    with col_1:
        st.text("   ")

    with col_2:
        st.image("images/banner3.jpg", width=1330)

    _, col_3, _ = st.columns([0.11, 0.65, 0.1])

    with col_3:
        st.subheader("Got questions? Ask our conversational agent 💬 :")
        prompt = st.text_input(label="", value="What is personal finance?")
        response = finance_bot(prompt=prompt)

        with st.spinner('Asking...'):
            time.sleep(0.5)
            st.success(response)

if __name__ == "__main__":
    main()
