from vertexai.preview.language_models import ChatModel, InputOutputTextPair



def finance_bot(temperature: float = .2, prompt: str = "What is personal finance?" ) -> None:

    chat_model = ChatModel.from_pretrained("chat-bison@001")

    # TODO developer - override these parameters as needed:
    parameters = {
        "temperature": temperature,  # Temperature controls the degree of randomness in token selection.
        "max_output_tokens": 256,    # Token limit determines the maximum amount of text output.
        "top_p": 0.95,               # Tokens are selected from most probable to least until the sum of their probabilities equals the top_p value.
        "top_k": 40,                 # A top_k of 1 means the selected token is the most probable among all tokens.
    }

    chat = chat_model.start_chat(
        context="My name is Tanuki. You are a financial advisor, knowledgeable about economics and investments.",
        examples=[
            InputOutputTextPair(
                input_text='What is finance?',
                output_text='Finance is the study and discipline of money, currency and capital assets.',
            ),
        ]
    )

    #prompt = st.text_input(label="", value="What is personal finance?")
    response = chat.send_message(prompt, **parameters)
    #print(f"Response from Model: {response.text}")
    #st.write(response.text)
# [END aiplatform_sdk_chat]

    return response




def db_connect_bot(conn, id, admin=False):
    query = "DELETE FROM bot_kb WHERE (SECRET is FALSE AND id = " + id + ");"
    cur = conn.cursor()

    if admin:
        query = "DELETE FROM bot_kb WHERE id = " + id

    bot.logger.info("Deleting row of kb with id: %s", id)

    try:
        cur.execute(query)
    except Exception as e:
        bot.logger.error("Failed to delete row of kb with id '': %s" % e)

if __name__ == "__main__":
    response = finance_bot()
    print("response from func:", response)