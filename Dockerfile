FROM python:3.9-slim-bullseye
LABEL maintainer="William Arias"
COPY .  /app/ 
WORKDIR /app
EXPOSE 8501
RUN pip install -r requirements.txt 
ENTRYPOINT ["streamlit", "run"]
CMD ["app.py"]
