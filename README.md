# Ask Me Anything

Ask Me Anything is an application that allows users. It is mainly used to showcase [GitLab DUO](https://about.gitlab.com/gitlab-duo/). You can see how the features available in this project are used by watching the following video:

[![GitLab Duo - AI-Powered DevSecOps Demo](images/yt_video_screenshot.png)](https://youtu.be/LifJdU3Qagw)

## GitLab Duo

GitLab Duo is the suite of AI capabilities powering your workflows. The benefits of GitLab Duo are as follows:

* **AI-powered workflows**: Boost efficiency and reduce cycle times with the help of AI in every phase of the software development lifecycle.
* **Privacy-first, Enterprise-grade**: We lead with a privacy-first approach to help enterprises and regulated organizations adopt AI-powered workflows.
* **Single application**: A single application with built-in security to deliver more software faster, enabling executive visibility across value streams and preventing context switching.
* **Empower every user at every step**: From planning and code creation to testing, security, and monitoring, our AI-assisted workflows support developer, security, and ops teams.

GitLab Duo contains the following AI features (**Last updated 21-08-2023**):

| **Feature** | **Description** | **LLM** | **Availability** | **Maturity** |
| ----------- | --------------- | ------- | ---------------- | ------------ |
| **Code Suggestions** | Enables you to write code more efficiently by viewing code suggestions as you type. | Google Vertex Codey APIs | SaaS | Self-Managed | All Tiers | Beta Testing |
| **Suggested Reviewers** | Helps you receive faster and higher-quality reviews by automatically finding the right people to review a merge request. | GitLab to create an ML model for your project that is used to generate reviewers | SaaS only** | General Availability (GA) |
| **Value Stream Forecasting** | Predicts productivity metrics and identifies anomalies across your software development lifecycle. | SaaS only** | | Experimental |
| **Summarize Issue Comments** | Quickly gets everyone up to speed on lengthy conversations to ensure you are all on the same page. | OpenAI’s GPT-3 | SaaS only** | Experimental |
| **Summarize proposed merge request changes** | Helps merge request authors drive alignment and action by efficiently communicating the impact of their changes. | Google Vertex Codey APIs | SaaS only** | Experimental |
| **Explain this Vulnerability** | Helps developers remediate vulnerabilities more efficiently and uplevel their skills, enabling them to write more secure code. | Google Vertex Codey APIs, Anthropic’s claude model if degraded performance | SaaS only**, Ultimate tier | Beta Testing |
| **Summarize Merge Request Review** | Enables better handoffs between authors and reviewers and helps reviewers efficiently understand merge request suggestions. | Google Vertex Codey APIs | SaaS only** | Experimental |
| **Fill in merge request templates** | When creating a merge request you can now choose to generate a description for the merge request based on the contents of the template. | Google Vertex Codey APIs | SaaS only** | Experimental |
| **Generate Tests in Merge Requests** | Automates repetitive tasks and helps you catch bugs early. | Google Vertex Codey APIs | SaaS only** | Experimental |
| **GitLab Duo Chat** | Helps you quickly identify useful information in large volumes of text, such as documentation. | Anthropic’s claude model, Claude-instant-1.1, OpenAI’s text-embedding-ada-002 embeddings | SaaS only** | Experimental |
| **Explain this Code** | Allows you to get up to speed quickly by explaining source code. | Google Vertex Codey APIs | SaaS only** | Experimental |

## Demoing

If you intend to use this project in order to demo GitLab Duo then you have come to the right place. In order to set this demo up in your own account, the following is required:

* A [GitLab SaaS](https://docs.gitlab.com/ee/subscriptions/gitlab_com/) account with the [Ultimate tier subscription](https://about.gitlab.com/pricing/ultimate/)
* [Maintaniner/Owner](https://docs.gitlab.com/ee/user/permissions.html) role in a [top-level group](https://docs.gitlab.com/ee/user/group/)
* A GCP cluster (TODO)

### Demo Setup

In order to get started setting up this demo, you must do the following:

1. Enable [Code Suggestions](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html) and [Experimental AI features](https://docs.gitlab.com/ee/user/ai_features.html#experiment-ai-features) in your top-level group and user account. See links below for enablement guides:

    * [Enable Code Suggestions](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html#enable-code-suggestions-on-gitlab-saas)
    * [Enable Experiment Features](https://docs.gitlab.com/ee/user/group/manage.html#enable-experiment-features)
    * [Enable 3rd-party AI features](https://docs.gitlab.com/ee/user/group/manage.html#enable-third-party-ai-features)

2. [Import this repository by URL](https://docs.gitlab.com/ee/user/project/import/repo_by_url.html) into the top-level group where you enabled the Experimental AI features in the previous step. In the [import project page](https://gitlab.com/projects/new#import_project) you can fill in the following:

  * **Git repository URL**: https://gitlab.com/gitlab-de/use-cases/devsecops-platform/gitlab-vertex-ai/ask-me-anything.git
  * **Project name**: Ask Me Anything - `YOUR_NAME`
  * **Project URL**: Select the top-level group used in step 1.
  * **Visibility Level**: Public

After filling the above, click the **Create project** button. The import will begin, wait about 1 min and the project should be successfully imported.

3. In the imported project [add maintainer/owner access](https://docs.gitlab.com/ee/user/project/members/#add-users-to-a-project) for those who will be demoing.

4. [Add the following CICD variables]() to the project:

    * **GCP_CLOUD_RUN_SA**:
    * TODO

5. [Run a pipeline manually](https://docs.gitlab.com/ee/ci/pipelines/#run-a-pipeline-manually) on the **main** branch. The pipeline should complete in 1-5 minutes.

6. Open the application by pressing the **Open** button in the **production** [environment](https://docs.gitlab.com/ee/ci/environments/). You can access **Environments** from the project side-bar under the **Operate** section.

Now you should be good to go! See the section below to get familiar with GitLab Duo features, how they work and how to enable them in this demo environment.

### GitLab Duo Feature Setup Guide

This section shows various GitLab Duo AI features, how to set them up, and how they can be leveraged in this project.

#### AI Summary in Issues

This feature allows you to quickly gets everyone up to speed on lengthy conversations to ensure you are all on the same page.

1. Create an issue as seen in [Improve FAQ experience](https://gitlab.com/gitlab-de/use-cases/devsecops-platform/gitlab-vertex-ai/ask-me-anything/-/issues/2)

2. Populate the issue with various comments as seen in the MR. You can add what you like, but it is recommended to add detailed suggestions and descriptions.

3. Under the **Activity** section, press the **View Summary** button. The AI-generated summary should be content rich as follows:

    ```text
    Summary: Improvements for other factor devices
    * The long FAQ webpage is not user friendly.
    * Consider adding a chat bar so users can use natural language to ask questions.
    * Implement a search bar to query a knowledge base.
    * The experience in the website should be smooth, people should be able to just ask questions without complicated search bar filters.
    * Use a Large Language model LLM to improve the customer experience.
    ```

#### AI Code suggestions

Enables you to write code more efficiently by viewing code suggestions as you type.

1. Open the GitLab WebIDE

2. Open db.py

3. Begin typing and Code Suggestions will add results for you

---

This demo environment is maintained by [William Arias]() and [Fernando Diaz]()